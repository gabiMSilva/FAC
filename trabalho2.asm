#Exponenciação modular = (a ^ b) % c 
.data 
	mens1: .asciiz "\n\nDigite a base: "
	mens2: .asciiz "Digite o expoente: "
	mens3: .asciiz "Digite o módulo: "	
	mens4: .asciiz "A exponencial modular "
	mens5: .asciiz " elevado a "
	mens6: .asciiz " (mod "
	mens7: .asciiz ") eh: "
	mens8: .asciiz "O modulo nao eh primo."

.text

le_inteiro:
	#printa mens1
	li $v0, 4 
	la $a0, mens1
	syscall

	#lê primeiro inteiro
	li $v0, 5 
	syscall
	move $t0, $v0

	#printa mens2
	li $v0, 4 
	la $a0, mens2
	syscall

	li $v0, 5 #lê segundo inteiro
	syscall	
	move $t1, $v0

	li $v0, 4 #printa mens3
	la $a0, mens3
	syscall

	li $v0, 5 #lê terceiro inteiro
	syscall	
	move $t2, $v0

eh_primo:
	li $t3, 2 # i = 2
	add $v0, $zero, $zero # flag=0
	loop:
		slt $a0, $t3, $t2 # if (i<n){ $a0=1} else {$a0=0} 
		beq $a0, $zero, exit # if ($t3==0){go to exit}
		rem $a2, $t2, $t3 # mod(n/i)
		beq $a2, $zero, equal_zero # if (n%i==0) {go to exit}	
		addi $t3, $t3, 1 # i=i+1	
		j loop # go to loop
	equal_zero:
		li $v0, 1 # flag=1
	
	exit:
		bne $v0, $zero, imprime_erro # if (flag != 0) go to imprime_erro
		
calc_exp:
	add $t3, $zero, $zero #i=0
	li $t4, 1 #ans=1
	loop_pot:
		beq $t1, $zero, pot_zero #if (exp=0) go to pot_zero 
		slt $a0, $t3, $t1 #if (i<exp) $a0=1, else $a0=0
		beq $a0, $zero, rm_pot #if (i>=exp) go to exit_b 
		mul $t4, $t4, $t0 # ans = ans*n
		addi $t3, $t3, 1 #i++
		j loop_pot
	pot_zero:
		addi $t4, $zero, 1 #resp=1
	rm_pot:
		rem $t4, $t4, $t2
		move $v1, $t4
		j imprime_saida
				
imprime_erro:
	li $v0, 4 #printa mens 5
	la $a0, mens8
	syscall

	li $v0, 10 # chamada exit
	syscall

imprime_saida:
	li $v0, 4 #printa mens 4
	la $a0, mens4
	syscall

	li $v0, 1 #print the larger int number
	move $a0, $t0
	syscall
	
	li $v0, 4 #printa mens 5
	la $a0, mens5
	syscall
	
	li $v0, 1 #print the larger int number
	move $a0, $t1
	syscall
	
	li $v0, 4 #printa mens 6
	la $a0, mens6
	syscall
	
	li $v0, 1 #print the larger int number
	move $a0, $t2
	syscall

	li $v0, 4 #printa mens 7
	la $a0, mens7
	syscall
	
	li $v0, 1 #print the larger int number
	move $a0, $t4
	syscall
	
	li $v0, 10 # chamada exit
	syscall



