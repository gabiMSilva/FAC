#include <math.h>
#include <stdio.h>

void calc_erro(double xm, double n){
	double pot, erro;
	//calc_pot:
	pot = pow(xm, 3);
	erro = n - pot;

	printf("A raiz cúbica eh %lf. O erro estimado eh de %.17lf.\n", xm, erro);
}

int main(){

    double x1, n, x2, xm, erro=0.000000000000001;

    //le_float
    printf("Digite o número: ");
    scanf("%lf", &n);

    //calc_raiz
    x2 = 1.1;
    x1 = 0;
    xm = 0;

    while(1){
    	//loop:
    	if (fabs(x2 - x1) < erro){
    		//go to calc_erro
    		calc_erro(xm, n);
    		break;
    	} else {
	        xm = (double) (x2 + x1) / 2;
	        
	        if (xm*xm*xm - n > 0) {
	        	//else2:	           
	            if (x1*x1*x1 - n > 0){
	            	//else4:
	            	x1 = xm;	
	            } else {
	            	//if4:
	            	x2 = xm;	
	            }

	        } else {
	        	//if2:
	            if (x1*x1*x1 - n < 1){ 
	            	//else3
	            	x1 = xm;
	            } else {
	            	//if3:
	            	x2 = xm;            
	        	}
	        }
	    }                    
    }    
    return 0;
}