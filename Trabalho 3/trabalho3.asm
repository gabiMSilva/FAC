# calcular raiz cúbica de n e para isso
# encontrar x tal que x³ - n = 0

.data 
    mens1: .asciiz "\n\nDigite o número: "
    mens2: .asciiz ". O erro foi estimado em "
    mens3: .asciiz "\nA raiz cubica eh "
    const1: .double 1
    constm1: .double -1
    const2: .double 2
    erro: .double 0.000000000000001
    zero: .double 0
    const11: .double 1.1
.text
le_float:
    #printa mens1
    li  $v0, 4 
    la  $a0, mens1
    syscall
    #lê o número decimal
    li  $v0, 7
    syscall    
    mov.d $f20, $f0 # f20=n 
    
    
calc_raiz:
    l.d $f8, const2
    l.d $f2, const1
    l.d $f14, constm1
    l.d $f6, erro
    l.d $f12, const11 #x2=1.1
    l.d $f16, zero #x1=0
    l.d $f24, zero
    l.d $f10, zero #xm=0

 	loop:
    	sub.d  $f18, $f12, $f16 #f18 = x2-x1
    	abs.d    $f18, $f18 #f18 = |x2-x1|
    	c.lt.d $f18, $f6 
    	bc1t   calc_erro #if (|x2-x1| < erro) go to calc_erro
    
    	add.d $f10, $f16, $f12 #xm = x1 + x2
    	div.d $f10, $f10, $f8  #xm = xm / 2
    	
    	mul.d $f4, $f10, $f10 
    	mul.d $f4, $f4, $f10  #f4 = xm³
    	sub.d $f4, $f4, $f20   #f4 = xm³ - n
    	c.le.d $f4, $f24
    	bc1f else2 #if (xm³ - n > 0) go to else2
        if2:
    		mul.d $f4, $f16, $f16
    		mul.d $f4, $f4, $f16  #f4 = x1³
    		sub.d $f4, $f4, $f20   #f4 = x1³ - n
    		c.lt.d $f4, $f2       #if x1³ - n < 1 go to else3
    		bc1t else3
            	if3:
            		mov.d $f12, $f10 # x2 = xm
            		j loop
            	else3:
            		mov.d $f16, $f10 # x1 = xm
            		j loop
            
        else2:
            mul.d $f4, $f16, $f16
    		mul.d $f4, $f4, $f16  #f4 = x1³
    		sub.d $f4, $f4, $f20   #f4 = x1³ - n
    		c.le.d $f4, $f24       #if x1³ - n > 0 go to else4
    		bc1f else4
            	if4:
            		mov.d $f12, $f10 # x2 = xm
            		j loop
            	else4:
            		mov.d $f16, $f10 # x1 = xm
            		j loop

calc_erro:
      
    calc_pot:
    	mov.d  $f26, $f10 		#n ($f18) = resultado da raiz 
    	la   $a0, const1 		#a0 = 1	
    	l.d  $f4, ($a0) 		#ans = 1
    	add  $t3, $zero, $zero  #i=0
    	li   $t1, 3 	        #exp = 3
    	
        loop_pot:
        	slt   $a0, $t3, $t1 	     #if (i<exp) $a0=1, else $a0=0
        	beq   $a0, $zero, encontra_erro  #if (i>=exp) go to exit_b 
        	mul.d $f4, $f4, $f26 	     #ans = ans*n
        	addi  $t3, $t3, 1 	     	 #i++
        	j loop_pot		     		 #go to loop_pot
	
encontra_erro:
	#erro = ans - valor_inserido
	sub.d $f4, $f20, $f4 
	abs.d $f4, $f4

imprime_saida:
	#imprime mens3
	la    $a0, mens3
	li    $v0, 4
	syscall
	
	#printa número decimal
    	li    $v0, 3
    	mov.d $f12, $f10
    	syscall

    #imprime mens2
    la    $a0, mens2
    li    $v0, 4
    syscall
    
    #imprime erro
    mov.d $f12, $f4
    li    $v0, 3
    syscall
